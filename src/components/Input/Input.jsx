import './Input.scss'
import InputMask from 'react-input-mask';

export function Input(props) {

   function handleChange(event) {
      props.onChange(event.currentTarget.value)
   }
   return (
      <div >
         <InputMask
           mask="99.99.9999"
            className="inputview"
            placeholder="Введите дату рождения."
            onChange={handleChange}
         />
      </div>
   );
}

