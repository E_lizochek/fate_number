import './Button.scss'
export function Button(props) {

    return (
      <button 
      className="buttonView"
      type="button"
      name="Рассчитать"
      onClick={props.onClick}> Рассчитать </button>
    );
  }