function changeStringToNumber(arr) {
    let arrn = arr.map((item) => {
        return Number(item)
    })
    return arrn;
}

function sumOfElementInArray(arr) {
    let chislo = arr.reduce(function (sum, current) {
        return sum + current;
    }, 0)

    return chislo;
}

function handlerAmount(number) {
    let a = Math.trunc(number / 10)
    let b = number % 10
    let c = a + b

    if (c >= 10) {
        return handlerAmount(c)
    }
    return c

}

export function getDateOfLive(date) {
    let dataWithoutDot = date.replace(/[\s.,%]/g, '')

    const array = changeStringToNumber(dataWithoutDot.split(''))


    const sumNumbers = sumOfElementInArray(array)


    const oneNumber = handlerAmount(sumNumbers);

    return oneNumber
}
