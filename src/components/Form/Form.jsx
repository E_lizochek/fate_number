import React, { useState } from "react"
import { Input } from "../Input/Input"
import { Button } from "../Button/Button"
import { MEAN } from "../Describes/Describes"
import './Form.scss'
import { getDateOfLive } from "./fateNumber"

export function Form(props) {

    const [state, setState] = useState('');
    const [mean, setMean] = useState('');
    const [number, getNumber] = useState(null);

    function getInputValue(value) {
        setState(value)
    }

    function describe(data) {
        switch (data) {
            case 1:
                setMean(MEAN.one)
                break
            case 2:
                setMean(MEAN.two)
                break
            case 3:
                setMean(MEAN.three)
                break
            case 4:
                setMean(MEAN.four)
                break
            case 5:
                setMean(MEAN.five)
                break
            case 6:
                setMean(MEAN.six)
                break
            case 7:
                setMean(MEAN.seven)
                break
            case 8:
                setMean(MEAN.eight)
                break
            case 9:
                setMean(MEAN.nine)
                break
        }
    }

    function submit() {
        const data = getDateOfLive(state)
        getNumber(data)
        describe(data)
    }


    return (
        <div className="form">
            <span className="title"> Число судьбы: о чём расскажет Ваша дата рождения. </span>
            <Input onChange={getInputValue} />
            <Button onClick={submit} />
            <div className="containerMean">
                <span className="containerMainTitle"> {number && (`Ваше число судьбы - ${number} `)} </span>
                <br/>
                <span>{mean}</span>
            </div>
        </div>
    )
}

